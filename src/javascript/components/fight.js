import { controls } from '../../constants/controls';

const {
  PlayerOneAttack,
  PlayerOneBlock,
  PlayerTwoAttack,
  PlayerTwoBlock,
  PlayerOneCriticalHitCombination,
  PlayerTwoCriticalHitCombination,
} = controls;

const fightStatus = {
  pressedKeys: {},
  lastPlayerOneCrit: null,
  lastPlayerTwoCrit: null,
};

const isCritOnCooldown = (lastCrit) => lastCrit && Date.now() - lastCrit < 10000;

const getWinner = (firstFighter, secondFighter) => {
  if (firstFighter.currentHealth <= 0) {
    return secondFighter;
  } else if (secondFighter.currentHealth <= 0) {
    return firstFighter;
  }
  return null;
}

const updateFight = (firstFighter, secondFighter) => {
  // 1(Player)
  if (!fightStatus.pressedKeys[PlayerOneBlock]) {
    if (!isCritOnCooldown(fightStatus.lastPlayerOneCrit) && PlayerOneCriticalHitCombination.every((val) => fightStatus.pressedKeys[val])) {
      secondFighter.currentHealth -= getCritPower(firstFighter);
      fightStatus.lastPlayerOneCrit = Date.now();
    } else if (fightStatus.pressedKeys[PlayerTwoBlock] && fightStatus.pressedKeys[PlayerOneAttack]) {
      secondFighter.currentHealth -= getDamage(firstFighter, secondFighter);
    } else if (fightStatus.pressedKeys[PlayerOneAttack]) {
      secondFighter.currentHealth -= getHitPower(firstFighter);
    }
  }

  // 2(Player)
  if (!fightStatus.pressedKeys[PlayerTwoBlock]) {
    if (!isCritOnCooldown(fightStatus.lastPlayerTwoCrit) && PlayerTwoCriticalHitCombination.every((val) => fightStatus.pressedKeys[val])) {
      firstFighter.currentHealth -= getCritPower(secondFighter);
      fightStatus.lastPlayerTwoCrit = Date.now();
    } else if (fightStatus.pressedKeys[PlayerOneBlock] && fightStatus.pressedKeys[PlayerTwoAttack]) {
      firstFighter.currentHealth -= getDamage(secondFighter, firstFighter);
    } else if (fightStatus.pressedKeys[PlayerTwoAttack]) {
      firstFighter.currentHealth -= getHitPower(secondFighter);
    }
  }
  const playerOneHealthToShow = firstFighter.currentHealth > 0 ? firstFighter.currentHealth : 0;
  const playerTwoHealthToShow = secondFighter.currentHealth > 0 ? secondFighter.currentHealth : 0;

  document.getElementById('left-fighter-indicator').style.width = `${Math.ceil((playerOneHealthToShow * 100)/firstFighter.health)}%`;
  document.getElementById('right-fighter-indicator').style.width = `${Math.ceil((playerTwoHealthToShow * 100)/secondFighter.health)}%`;
} 

export function fight(firstFighter, secondFighter) {
  firstFighter.currentHealth = firstFighter.health;
  secondFighter.currentHealth = secondFighter.health;

  return new Promise(resolve => {
    document.addEventListener("keydown", function onKeyDown ({ code }) {
      switch (code) {
        case PlayerOneAttack:
        fightStatus.pressedKeys[PlayerOneAttack] = true;
        break;
        case PlayerOneBlock:
        fightStatus.pressedKeys[PlayerOneBlock] = true;
        break;
        case PlayerTwoAttack:
        fightStatus.pressedKeys[PlayerTwoAttack] = true;
        break;
        case PlayerTwoBlock:
        fightStatus.pressedKeys[PlayerTwoBlock] = true;
        break;
        default:
          if (PlayerOneCriticalHitCombination.some(key => key === code) || PlayerTwoCriticalHitCombination.some(key => key === code)) {
            fightStatus.pressedKeys[code] = true;
          }
      }
      updateFight(firstFighter, secondFighter);
      const winner = getWinner(firstFighter, secondFighter);
      winner && resolve(winner);
    });
    document.addEventListener("keyup", ({ code }) => {
      fightStatus.pressedKeys[code] = false;
    });
  });
}

export function getDamage(attacker, defender) {
  const dmg = getBlockPower(defender) - getHitPower(attacker);
  return dmg > 0 ? dmg : 0;
}

export function getCritPower(fighter) {
  return 2 * fighter.attack;
}

export function getHitPower(fighter) {
  return fighter.attack * (1 + Math.random());
}

export function getBlockPower(fighter) {
  return fighter.defense * (1 + Math.random());
}
